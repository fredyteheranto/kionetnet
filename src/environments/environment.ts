// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyAPjOL_An__C10b1Td5ZSSww7AQds9jPU0',
    authDomain: 'kio-networking.firebaseapp.com',
    databaseURL: 'https://kio-networking.firebaseio.com',
    projectId: 'kio-networking',
    storageBucket: 'kio-networking.appspot.com',
    messagingSenderId: '82802307317',
    appId: '1:82802307317:web:6427d914f1ada8fc'
  }
};


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
