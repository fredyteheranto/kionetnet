const proxy = [{
  context: '/api',
  target: 'https://kai.kioapps.com',
  pathRewrite: {
    '^/api': ''
  }
}];
module.exports = proxy;
