//var converter = new showdown.Converter();
//converter.setOption('openLinksInNewWindow', true);
var sound_mute = false;
var repls_highPriority = false;
var cont = 0;

var Botkit = {
  config: {
    ws_url: (location.protocol === 'https:' ? 'wss' : 'ws') + '://' + location.host,
    reconnect_timeout: 3000,
    max_reconnect: 5
  },
  options: {
    sound: true,
    use_sockets: true
  },
  reconnect_count: 0,
  guid: null,
  current_user: null,
  botConnected: false,
  on: function (event, handler) {
    console.log('okokokok', event)
    this.message_window.addEventListener(event, function (evt) {
      handler(evt.detail);
    });
  },
  trigger: function (event, details) {
    var event = new CustomEvent(event, {
      detail: details
    });
    this.message_window.dispatchEvent(event);
  },
  request: function (url, body) {
    that = this;
    return new Promise(function (resolve, reject) {
      var xmlhttp = new XMLHttpRequest();

      xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == XMLHttpRequest.DONE) {
          if (xmlhttp.status == 200) {
            var response = xmlhttp.responseText;
            var message = null;
            try {
              message = JSON.parse(response);
            } catch (err) {
              reject(err);
              return;
            }
            resolve(message);
          } else {
            reject(new Error('status_' + xmlhttp.status));
          }
        }
      };

      xmlhttp.open("POST", url, true);
      xmlhttp.setRequestHeader("Content-Type", "application/json");
      xmlhttp.send(JSON.stringify(body));
    });

  },
  send: function (text, e) {
    if (e) e.preventDefault();
    if (!text) {
      return;
    }
    var message = {
      type: 'outgoing',
      text: text
    };

    this.clearReplies();
    that.renderMessage(message);

    that.deliverMessage({
      type: 'message',
      text: text,
      user: this.guid,
      channel: this.options.use_sockets ? 'socket' : 'webhook'
    });

    this.input.value = '';

    this.trigger('sent', message);

    return false;
  },
  deliverMessage: function (message) {
    if (this.options.use_sockets) {
      this.socket.send(JSON.stringify(message));
    } else {
      this.webhook(message);
    }
  },
  getHistory: function (guid) {
    that = this;
    if (that.guid) {
      that.request('/botkit/history', {
        user: that.guid
      }).then(function (history) {
        if (history.success) {
          that.trigger('history_loaded', history.history);
        } else {
          that.trigger('history_error', new Error(history.error));
        }
      }).catch(function (err) {
        that.trigger('history_error', err);
      });
    }
  },
  webhook: function (message) {
    that = this;

    that.request('/botkit/receive', message).then(function (message) {
      that.trigger(message.type, message);
    }).catch(function (err) {
      that.trigger('webhook_error', err);
    });

  },
  connect: function (user) {

    var that = this;

    if (user && user.id) {
      Botkit.setCookie('botkit_guid', user.id, 1);

      user.timezone_offset = new Date().getTimezoneOffset();
      that.current_user = user;
      console.log('CONNECT WITH USER', user);
    }
    ws_url = Botkit.getCookie('botkit_wsbot');
    if (ws_url) {
      that.config.ws_url = (location.protocol === 'https:' ? 'wss' : 'ws') + '://' + ws_url;
    }
    // connect to the chat server!
    if (that.options.use_sockets) {
      that.connectWebsocket(that.config.ws_url);
    } else {
      that.connectWebhook();
    }

  },
  connectWebhook: function () {
    var that = this;
    if (Botkit.getCookie('botkit_guid')) {
      that.guid = Botkit.getCookie('botkit_guid');
      connectEvent = 'welcome_back';
    } else {
      that.guid = that.generate_guid();
      Botkit.setCookie('botkit_guid', that.guid, 1);
    }

    that.getHistory();

    // connect immediately
    that.trigger('connected', {});
    that.webhook({
      type: connectEvent,
      user: that.guid,
      channel: 'webhook',
    });

  },
  connectWebsocket: function (ws_url) {
    var that = this;
    // Create WebSocket connection.
    that.socket = new WebSocket(ws_url);

    var connectEvent = 'hello';
    if (Botkit.getCookie('botkit_guid')) {
      that.guid = Botkit.getCookie('botkit_guid');
      connectEvent = 'welcome_back';
    } else {
      that.guid = that.generate_guid();
      Botkit.setCookie('botkit_guid', that.guid, 1);
    }

    that.getHistory();

    // Connection opened
    that.socket.addEventListener('open', function (event) {
      console.log('CONNECTED TO SOCKET');
      //that.reconnect_count = 0;
      that.trigger('connected', event);
      that.deliverMessage({
        type: connectEvent,
        user: that.guid,
        channel: 'socket',
        user_profile: that.current_user ? that.current_user : null,
      });
    });

    that.socket.addEventListener('error', function (event) {
      console.error('ERROR', event);
    });

    that.socket.addEventListener('close', function (event) {
      console.log('SOCKET CLOSED!');
      this.botConnected = false;
      that.trigger('disconnected', event);
      if (that.reconnect_count < that.config.max_reconnect) {
        setTimeout(function () {
          console.log('RECONNECTING ATTEMPT ', ++that.reconnect_count);
          that.connectWebsocket(that.config.ws_url);
        }, that.config.reconnect_timeout);
      } else {
        that.message_window.className = 'offline';
        window.location.replace("/logout")
      }
    });

    // Listen for messages
    that.socket.addEventListener('message', function (event) {
      this.reconnect_count = 0;
      this.botConnected = true;
      var message = null;
      try {
        message = JSON.parse(event.data);
      } catch (err) {
        that.trigger('socket_error', err);
        return;
      }

      that.trigger(message.type, message);
    });
  },
  clearReplies: function (ulid) {
    if (ulid) document.getElementById(ulid).innerHTML = '';
    this.replies.innerHTML = '';
  },
  quickReply: function (payload) {
    repls_highPriority = false;
    this.send(payload);
  },
  focus: function () {
    this.input.focus();
  },
  renderMessage: function (message) {
    if (!that.next_line) {
      that.next_line = document.createElement('div');
      that.message_list.appendChild(that.next_line);
    }
    if (message.text) {
      // message.html = converter.makeHtml(message.text);
    }
    if (message.quick_replies) {
      cont++;
      message.html += '<div id="message_replies"><ul id="ulid' + cont + '">';
      for (var r = 0; r < message.quick_replies.length; r++) {
        message.html += "<li><a href=\"#\" onclick=\"Botkit.send('" + message.quick_replies[r].payload + "' , event); Botkit.clearReplies('ulid" + cont + "')\">" + message.quick_replies[r].title + "</a></li>"
      }
      message.html += '</ul></div>'
    }

    that.next_line.innerHTML = that.message_template({
      message: message
    });
    if (!message.isTyping) {
      delete(that.next_line);
    }
    //Disabling input
    if (message.disable_input) {
      that.input.disabled = true;
    } else {
      that.input.disabled = false;
    }

  },
  renderFileMessage: function (message) {
    if (!that.next_line) {
      that.next_line = document.createElement('div');
      that.message_list.appendChild(that.next_line);
    }
    if (message.text) {
      //message.html = '<a download="'+message.text["0"].name+'.'+message.text["0"].ext+'" href=https://kiotron.kioapps.com/file/'+ message.text["0"].id+'>'+message.text["0"].name+'.'+message.text["0"].ext+'</a>'
      message.html = '<a href="' + message.text["0"].url.replace('/api', '') + '" target="_blank" >' + message.text["0"].name + '.' + message.text["0"].ext + '</a>'
    }

    that.next_line.innerHTML = that.message_template({
      message: message
    });
    if (!message.isTyping) {
      delete(that.next_line);
    }


    /*         var ext = message.file.base64file.type;
        var file = atob(message.file.base64file);
          var download = $('<a class="btn btn-default" download="base64"/>').text('Download');
          download.click(function() {
            var base64Str = $('#input').val();
            download.attr('href', 'data:application/octet-stream;base64,' + base64Str);
          }); */


  },
  triggerScript: function (script, thread) {
    this.deliverMessage({
      type: 'trigger',
      user: this.guid,
      channel: 'socket',
      script: script,
      thread: thread
    });
  },
  identifyUser: function (user) {

    user.timezone_offset = new Date().getTimezoneOffset();

    this.guid = user.id;
    Botkit.setCookie('botkit_guid', user.id, 1);

    this.current_user = user;

    this.deliverMessage({
      type: 'identify',
      user: this.guid,
      channel: 'socket',
      user_profile: user,
    });
  },
  receiveCommand: function (event) {
    switch (event.data.name) {
      case 'trigger':
        // tell Botkit to trigger a specific script/thread
        console.log('TRIGGER', event.data.script, event.data.thread);
        Botkit.triggerScript(event.data.script, event.data.thread);
        break;
      case 'identify':
        // link this account info to this user
        console.log('IDENTIFY', event.data.user);
        Botkit.identifyUser(event.data.user);
        break;
      case 'connect':
        // link this account info to this user
        Botkit.connect(event.data.user);
        break;
      default:
        console.log('UNKNOWN COMMAND', event.data);
    }
  },
  sendEvent: function (event) {

    if (this.parent_window) {
      this.parent_window.postMessage(event, '*');
    }

  },
  setCookie: function (cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
  },
  getCookie: function (cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  },
  generate_guid: function () {
    function s4() {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
      s4() + '-' + s4() + s4() + s4();
  },
  boot: function (user) {

    console.log('Booting up');

    var that = this;


    that.message_window = document.getElementById("message_window");

    that.message_list = document.getElementById("message_list");

    var source = document.getElementById('message_template').innerHTML;
    that.message_template = Handlebars.compile(source);

    that.replies = document.getElementById('message_replies');

    that.input = document.getElementById('messenger_input');

    that.focus();

    that.on('connected', function () {
      that.message_window.className = 'connected';
      that.input.disabled = false;
      that.sendEvent({
        name: 'connected'
      });
    })

    that.on('disconnected', function () {
      that.message_window.className = 'disconnected';
      that.input.disabled = true;
    });

    that.on('webhook_error', function (err) {

      alert('Error sending message!');
      console.error('Webhook Error', err);

    });

    that.on('typing', function () {
      that.clearReplies();
      that.renderMessage({
        isTyping: true
      });
    });

    that.on('sent', function () {
      if (that.options.sound && !sound_mute) {
        var audio = new Audio('sent.mp3');
        audio.play();
      }
    });

    that.on('message', function () {
      if (that.options.sound && !sound_mute) {
        var audio = new Audio('beep.mp3');
        playPromise = audio.play();
        if (playPromise !== undefined) {
          playPromise.then(_ => {})
            .catch(error => {
              console.log(error)
            })
        };
      }
    });

    that.on('message', function (message) {
      //Del back se indica que no se debe reproducir
      if (message.TTS == undefined) message.TTS = true;

      if (message.text && !sound_mute && message.TTS) {
        AWS.config.region = 'us-east-2';
        AWS.config.credentials = new AWS.CognitoIdentityCredentials({
          IdentityPoolId: 'us-east-2:505a02a2-c236-4344-a64b-fe1da61069f0'
        });

        var speechParams = {
          OutputFormat: "mp3",
          SampleRate: "16000",
          Text: "",
          TextType: "text",
          VoiceId: "Miguel"
        };

        speechParams.Text = message.text;
        //speechParams.Text = "actualmente ejecuto algoritmos de procesamiento de lenguaje natural, redes neuronales profundas, reconocimiento facial y en fase de entrenamiento detección de anomalías";
        //speechParams.Text = "¿En qué te puedo ayudar?";
        var polly = new AWS.Polly({
          apiVersion: '2016-06-10'
        });
        var signer = new AWS.Polly.Presigner(speechParams, polly)
        signer.getSynthesizeSpeechUrl(speechParams, function (error, url) {
          if (error)
            console.log(error);
          else {
            var audio = document.getElementById('audioPlayback');
            audioSource.src = url;
            //audio.src = url;
            audio.load();

            /*                var audio2 = new Audio(url);
                          seg=audio2.duration;
                          halaloop(seg) */
            ;

          }
        });
        that.renderMessage(message);
        if (message.file) {
          message2 = message;
          message2.text = message.file;
          message2.file = null
          that.renderFileMessage(message2);
        }
        if (message.videoID != undefined) {
          console.log("change video to " + message.videoID)
          changeGadget(message.videoID);
        }
      } else {
        that.renderMessage(message);
        if (message.file) {
          message2 = message;
          message2.text = message.file;
          message2.file = null
          that.renderFileMessage(message2);
        }
        if (message.videoID == "Despedida_6s" && sound_mute) window.location.replace("/logout");
      }

    });

    that.on('message', function (message) {
      if (message.goto_link) {
        window.location = message.goto_link;
      }
    });

    /*     that.on('message', function(message) {
          if (!repls_highPriority){
            that.clearReplies();
            if (message.priority!=undefined && message.priority=="high"){
              repls_highPriority=true;
            }
            if (message.quick_replies) {

              var list = document.createElement('ul');

              var elements = [];
              for (var r = 0; r < message.quick_replies.length; r++) {
                (function(reply) {

                  var li = document.createElement('li');
                  var el = document.createElement('a');
                  el.innerHTML = reply.title;
                  el.href = '#';

                  el.onclick = function() {
                    that.quickReply(reply.payload);
                  }

                  li.appendChild(el);
                  list.appendChild(li);
                  elements.push(li);

                })(message.quick_replies[r]);
              }

              that.replies.appendChild(list);

              // uncomment this code if you want your quick replies to scroll horizontally instead of stacking
              // var width = 0;
              // // resize this element so it will scroll horizontally
              // for (var e = 0; e < elements.length; e++) {
              //     width = width + elements[e].offsetWidth + 18;
              // }
              // list.style.width = width + 'px';

              if (message.disable_input) {
                that.input.disabled = true;
              } else {
                that.input.disabled = false;
              }
            } else {
              that.input.disabled = false;
            }
          }
        });
     */
    that.on('history_loaded', function (history) {
      if (history) {
        for (var m = 0; m < history.length; m++) {
          that.renderMessage({
            text: history[m].text,
            type: history[m].type == 'message_received' ? 'outgoing' : 'incoming', // set appropriate CSS class
          });
        }
      }
    });


    if (window.self !== window.top) {
      // this is embedded in an iframe.
      // send a message to the master frame to tell it that the chat client is ready
      // do NOT automatically connect... rather wait for the connect command.
      that.parent_window = window.parent;
      window.addEventListener("message", that.receiveCommand, false);
      that.sendEvent({
        type: 'event',
        name: 'booted'
      });
      console.log('Messenger booted in embedded mode');

    } else {

      console.log('Messenger booted in stand-alone mode');
      // this is a stand-alone client. connect immediately.
      that.connect(user);
    }

    return that;
  }
};


/*     (function() {
      // your page initialization code here
      // the DOM will be available here
      Botkit.boot();
    })(); */

function soundMute() {

  if (sound_mute) {
    sound_mute = false;
    document.getElementById("btn_mute").innerHTML = "Mute"
  } else {
    sound_mute = true;
    document.getElementById("btn_mute").innerHTML = "Unmute"
  }
}
/* function clearDivReplies(ulid){
  document.getElementById(ulid).innerHTML=''
} */
