var vid_toplay = "#vid_talk";
var aud_playing = false;
var loaded = 0;
var salida = false;

$(document).ready(function () {
  // -----ELEMENTOS DE VIDEO
  //-----Cheking video loads event to hide loading gif
  $("#vid_entry").on('loadedmetadata', function () {
    loaded++;
    chekload();
  });
  $("#vid_standby").on('loadedmetadata', function () {
    this.muted = true;
    loaded++;
    chekload();
  });
  $("#vid_talk").on('loadedmetadata', function () {
    loaded++;
    chekload();
  });

  //controlling video events

  $("#vid_entry").on('ended', function () {

    Botkit.boot();
    $("#vid_entry").hide();
    $("#div_vid_entry").hide();

    $("#vid_standby").show();
    $("#vid_standby").muted = true;
    $("#vid_standby").get(0).play();

  });

  $("#vid_standby").on('ended', function () {
    $("#vid_standby").get(0).play();
    this.muted = true;
  });

  $("#vid_gadget").on('ended', function () {
    if (salida) {
      window.location.replace("/logout");
    }
    if (aud_playing) {
      $("#div_vid_entry").hide();
      $("#vid_gadget").hide();
      $("#vid_talk").show();
      $("#vid_talk").get(0).play();
    }
    vid_toplay = "#vid_talk";
  });

  $("#vid_talk").on('ended', function () {
    $("#vid_talk").get(0).play();
  });

  //--------ELEMENTOS DE AUDIO
  $("#audioPlayback").on('loadedmetadata', function () {

    $("#vid_standby").get(0).pause();
    $("#vid_standby").hide();
    if (vid_toplay == "#vid_gadget") $("#div_vid_entry").show();
    $(vid_toplay).show();
    $(vid_toplay).get(0).play();

    //Satart to play TTS
    playPromise = this.play();

    if (playPromise !== undefined) {
      playPromise.then(_ => {
          // Automatic playback started!
          // Show playing UI.
          aud_playing = true;
        })
        .catch(error => {
          // Auto-play was prevented
          // Show paused UI.
          if (!salida) {
            $("#vid_standby").get(0).pause();
            aud_playing = false;
            $("#div_vid_entry").hide();
            $("#vid_talk").hide();
            $("#vid_gadget").hide();
            $("#vid_standby").show();
            $("#vid_standby").get(0).play();
          }
        });

    }

  })

  $("#audioPlayback").on('ended', function () {
    if (!salida) {
      $("#vid_standby").get(0).pause();
      aud_playing = false;
      $("#div_vid_entry").hide();
      $("#vid_talk").hide();
      $("#vid_gadget").hide();
      $("#vid_standby").show();
      $("#vid_standby").get(0).play();
    }
  });

});

function chekload() {
  if (loaded == 2) {
    $("#dimScreen").hide();
    $("#vid_entry").get(0).play();
    //document.getElementById("vid_entry").play()
  }
}

function changeGadget(vidID) {
  console.log("changeGadget");
  vid_toplay = "#vid_gadget";
  if (vidID == "Despedida_6s" || vidID == "Cuadrado_Despedida_3s") salida = true;
  source = '/video/' + decodeURI(vidID) + '.webm';
  $('#vid_gadget').attr('src', source);
  $("#vid_gadget").get(0).load();
}

function salir() {
  document.getElementById("btnLogout").disabled = true;
  document.getElementById('messenger_input').disabled = true;
  if (!salida) {
    salida = true;
    try {
      Botkit.send('bye', event);
    } catch (error) {
      window.location.replace("/logout");
    }
  }

}
