import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { AuthGuardService } from './auth/auth-guard.service';



const routes: Routes = [

  { path: 'login', component: LoginComponent },
  {
    path: '',
    loadChildren: './splash/splash.module#SplashModule'
  }, {
    path: 'quienes-somos',
    loadChildren: './slides/slides.module#SlidesModule'
  }, {
    path: 'bot',
    loadChildren: './bot/bot.module#BotModule',
    canLoad: [AuthGuardService]
  },
  { path: '**', redirectTo: '' },

];


@NgModule({

  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]

})
export class AppRoutingModule { }
