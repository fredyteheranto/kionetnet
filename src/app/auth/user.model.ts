


export class User {

    public nombre: string;
    public empresa: string;
    public telefono: string;
    public correo_electronico: any;
    public uid: string;



    constructor(obj: DataObj) {
        this.nombre = obj && obj.nombre || null;
        this.uid = obj && obj.uid || null;
        this.correo_electronico = obj && obj.correo_electronico || null;
        this.telefono = obj && obj.telefono || null;

    }

}

interface DataObj {
    uid: string;
    correo_electronico: string;
    nombre: string;
    telefono: string;
}

