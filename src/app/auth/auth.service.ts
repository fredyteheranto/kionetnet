import { Injectable } from '@angular/core';

import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore } from 'angularfire2/firestore';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import {
  ActivarLoadingAction,
  DesactivarLoadingAction
} from '../shared/ui.accions';
import * as firebase from 'firebase/app';
import { map } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { User } from './user.model';
import { AppState } from '../app.reducer';
import { SetUserAction, UnsetUserAction } from './auth.actions';
import { Subscription } from 'rxjs';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';


declare var $: any;


@Injectable({
  providedIn: 'root'
})
export class AuthServic {
  protected api = 'https://kai.kioapps.com/api/';
  public headers = new HttpHeaders({
    'Content-Type': 'application/json'
  },
  );


  private userSubscription: Subscription = new Subscription();
  private usuario: User;
  public estado;

  constructor(private afAuth: AngularFireAuth,
    private router: Router,
    private afDB: AngularFirestore,
    private store: Store<AppState>,
    public http: HttpClient) {
    this.estado = false;
  }

  public senPost(endpoint: any, form: any) {
    return this.http.post(`${this.api}${endpoint}`, form, { headers: this.headers });
  }
  public sendget(endpoint: any) {
    return this.http.get(`${this.api}${endpoint}`, { headers: this.headers });
  }

  public sendgetAuth(endpoint: any, token: any) {
    const hder = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: token

    },
    );
    return this.http.get(endpoint, { headers: hder });
  }

  initAuthListener() {
    this.afAuth.authState.subscribe((fbUser: firebase.User) => {
      if (fbUser) {
        this.userSubscription = this.afDB.doc(`${fbUser.uid}/usuario`).valueChanges()
          .subscribe((usuarioObj: any) => {
            const newUser = new User(usuarioObj);
            this.store.dispatch(new SetUserAction(newUser));
            this.usuario = newUser;
          });
      } else {
        this.usuario = null;
        this.userSubscription.unsubscribe();
      }
    });
  }


  crearUsuario(data: any) {

    this.store.dispatch(new ActivarLoadingAction());

    this.afAuth.auth
      .createUserWithEmailAndPassword(data.correo_electronico, data.contrasena)
      .then(resp => {
        /*
        nombre: '',
        empresa: '',
        telefono: '',
        correo_electronico: '',
        contrasena: '',
        Confirmar_contrasena: ''

        */

        // console.log(resp);
        const user: User = {
          uid: resp.user.uid,
          nombre: data.nombre,
          empresa: data.empresa,
          telefono: data.telefono,
          correo_electronico: resp.user.email
        };

        this.afDB.doc(`${user.uid}/usuario`)
          .set(user)
          .then(() => {

            // Swal('Crear Usuario', 'Usuario Creado con exito', 'success');
            this.router.navigate(['/bot']);
            this.store.dispatch(new DesactivarLoadingAction());
          });

      })
      .catch(error => {
        console.error(error);
        this.store.dispatch(new DesactivarLoadingAction());
        Swal('Error en el login', error.message, 'error');
      });
  }

  signInWithGoogle(user) {
    console.log('usuarioooooo', user);
    if (user !== null) {

      console.log(this.estado);
      this.estado = true;
      // this.estado = true;
      // this.store.dispatch(new DesactivarLoadingAction());

      const send = {
        autentication: 'SSO',
        user_id: user.email,
        user_pass: '',
        idtoken: user.idToken// resp.credential["idToken"]
      };
      this.senPost('login', send).subscribe((res: any) => {
        console.log(res);
        if (res) {

          this.SaveStorage('LoginAPi', res);
          this.router.navigateByUrl('/bot');


        }

      },
        error => {
          Swal('Error', error.error.msg, 'error');
          this.estado = false;
          console.log('dara', error);
        });
    } else {
      this.estado = false;
    }

  }
  login(email: string, password: string) {


    this.store.dispatch(new ActivarLoadingAction());

    this.afAuth.auth
      .signInWithEmailAndPassword(email, password)
      .then(resp => {

        console.log(resp);
        this.SaveStorage('LoginFire', resp);
        this.store.dispatch(new DesactivarLoadingAction());
        this.router.navigate(['/bot']);

      })
      .catch(error => {
        console.error(error);
        this.store.dispatch(new DesactivarLoadingAction());
        Swal('Error en el login', error.message, 'error');
      });

  }

  logout() {

    this.router.navigate(['/']);
    this.store.dispatch(new UnsetUserAction());
    this.borroTodo();



  }

  SaveStorage(key, data) {
    if (key && data) {
      localStorage.setItem(key, JSON.stringify(data));
    }
    // Guardar

  }
  getStorage(key) {
    const data = JSON.parse(localStorage.getItem(key));
    return data;
  }
  borrarSt(key) {
    localStorage.removeItem(key);
    return 'Borrado';

  }

  borroTodo() {
    localStorage.clear();
    this.estado = false;

  }

  isAuth() {

    if (!this.estado) {
      this.estado = false;
    }

    return this.estado;

  }

  getUsuario() {
    return { ...this.usuario };
  }

}
