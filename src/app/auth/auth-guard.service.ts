import { Injectable } from '@angular/core';
import { CanActivate, CanLoad } from '@angular/router';
import { AuthServic} from './auth.service';
import { take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate, CanLoad {

  constructor(public authService: AuthServic) { }

  canActivate() {
    return this.authService.estado;
  }


  canLoad() {
    return this.authService.estado;
  }


}
