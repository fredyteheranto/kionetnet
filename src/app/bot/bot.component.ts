

import { AuthServic } from './../auth/auth.service';
import { KioService } from './../services/kio.service';

import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
declare var $: any;
declare var AWS: any;

import { Subscription } from 'rxjs';
import { Router } from '@angular/router';


const cleanUri = location.protocol + '//' + location.host + location.pathname;
export interface FileEvent {
  sessionId: string;
  path: string;
}

@Component({
  selector: 'app-bot',
  templateUrl: './bot.component.html',
  styleUrls: ['./bot.component.scss']
})


export class BotComponent implements OnInit, OnDestroy {
  public userLowe;
  public wsUri = 'wss://kai.kioapps.com';
  private ws = new WebSocket(this.wsUri);
  public receivedMessages: string[] = [];
  private topicSubscription: Subscription;
  files: Array<FileEvent> = [];
  public mensajeChat = [];
  public video = 'Cuadrado_Entrada_Kiotron_6s.gif';
  public hvideo = true;
  public hablaVid = false;
  public stanhvideo = false;
  public alto = String(document.body.clientHeight - 80) + 'px';
  public button;
  public container: HTMLElement;
  public mataclick = '';



  // this.ws = new WebSocket(this.wsUri);


  constructor(private route: ActivatedRoute, public sp: KioService, public servicio: AuthServic, public router: Router) {
    AWS.config.region = 'us-east-2';
    AWS.config.credentials = new AWS.CognitoIdentityCredentials({ IdentityPoolId: 'us-east-2:505a02a2-c236-4344-a64b-fe1da61069f0' });
    this.mensajeChat = [];



    //  this.ws = new WebSocket(this.wsUri);
    //  this.userLowe = this.servicio.getStorage('LoginAPi');
    this.showSearchButton = true;
    this.speechData = '';

    this.userData = {
      userapi: this.servicio.getStorage('LoginAPi'),
      userfire: this.servicio.getStorage('LoginFire')
    };

    console.log(this.wsUri, this.userLowe);
    this.route.queryParams.subscribe(params => {
      // if (params.link === 'faqs') {
      this.bot = (params.link === 'faqs') ? false : true;
      this.condicions = (params.link === 'terminos') ? false : true;

      if (params.link === 'token') {
        $(document).ready(() => {
          setTimeout(() => {
            $('#ModalToken').modal('show');
          }, 300);
        });
      }
      if (params.link === 'mensajes') {
        $(document).ready(() => {
          setTimeout(() => {
            $('#ModalMensaje').modal('show');
            this.getMensajes();
          }, 300);
        });
        this.router.navigateByUrl('/bot?link=');
      }

    });

    const cleanUri = location.protocol + '//' + location.host + location.pathname;
    window.history.replaceState({}, document.title, cleanUri);

    this.ws.onmessage = (me: MessageEvent) => {
      // var msgDiv = document.getElementById("message_list");
      //    msgDiv.scrollTop = msgDiv.scrollHeight;
      const fe = JSON.parse(me.data);
      console.log(fe.code);
      let text: any = (fe.code === '200') ? fe.text : 'Adios';

      if (fe.code === '401' && text === 'Adios') {
        this.Adios();

      } else {
        if (fe.TTS) {
          this.audioVoice(text);
        }

      }

      // this.video = (fe.videoID) ? fe.videoID + '.gif' : 'Cuadrado_Idle_3s.gif';


      this.sp.DestroySpeechObject();
      console.log(fe.videoID);




      if (fe.text === 'Muy buen día' || fe.text === 'Hola, Bienvenido a KIO Networks') {
        this.video = 'Cuadrado_Hablando_3s.gif';
      } else if (fe.videoID) {
        this.video = fe.videoID + '.gif';
      } else {
        this.video = 'Cuadrado_Hablando_3s.gif';
      }


      // this.audioVoice(fe.text);
      this.mensajeChat = [...this.mensajeChat, {
        tipo: 'bot',
        mensaje: text,
        videoID: fe.videoID,
        type: fe.type,
        file: (fe.file) ? fe.file : null,
        quick_replies: (fe.quick_replies) ? fe.quick_replies : null,
      }];
      $('#message_list').stop().animate({
        scrollTop: $('#message_list')[0].scrollHeight
      }, 800);

      if (fe.code === '401' || fe.videoID === 'Cuadrado_Despedida_3s') {
        this.servicio.logout();
      }
      console.log('traigo algo?', fe.file, fe.file !== undefined, fe.file !== null);
      if (fe.file !== undefined && fe.file) {
        for (let index = 0; index < fe.file.length; index++) {
          this.servicio.sendgetAuth(fe.file[index].url, this.userData.userapi.authorization).subscribe(
            (res: any) => {
              console.log('que traje', res);
            },
            error => {
              console.log('di error', error);
            });

        }


      }


      // s console.log(JSON.stringify(fe), me.data);


    };



  }
  public rol: boolean;
  public bot = true;
  public Height = 0;
  public faq = true;
  public condicions = false;
  public imgaudio = 'audio';
  public loaded = 0;
  public maudio;
  public showSearchButton: boolean;
  public speechData: string;
  public userData;
  public tiempo = 0

  public audio: string;
  public message = {
    type: '',
    user: '',
    chanel: 'socket',
    text: 'HOla fred'
  };
  public altoH = window.innerHeight;


  ngOnInit() {

    this.video = 'Cuadrado_Idle_3s.gif';



    setTimeout(() => {


      // this.alto = String(document.body.clientHeight - 80) + 'px';
      this.Height = document.documentElement.offsetHeight;
      this.rol = true;
      console.log(this.rol, this.Height);
      $('#ModalReg').modal('hide');
      console.log('Todo da', this.userData);
      this.ws.onopen = (e) => { console.log('Welcome - status ' + JSON.stringify(e)); };
      const host = (this.userData.userapi !== null) ? this.userData.userapi.botkit_wsbot : 'kai.kioapps.com';
      this.wsUri = 'wss://' + host;

      let newSMS = {
        "type": "hello",
        "user": this.userData.userapi.user_id,
        "channel": "socket",
      }

      const myJSONT = JSON.stringify(newSMS);
      this.ws.send(myJSONT);
      // this.sendMsg('Hello');
    }, 800);
  }



  audioVoice(txt) {

    const params = {
      OutputFormat: 'mp3',
      SampleRate: '16000',
      Text: txt,
      TextType: 'text',
      VoiceId: 'Miguel'
    };

    const polly = new AWS.Polly({ apiVersion: '2016-06-10' });
    const signer = new AWS.Polly.Presigner(params, polly);

    signer.getSynthesizeSpeechUrl(params, (err: { stack: any; }, url: any) => {
      if (err) {
        console.log(err, err.stack);
      } else {
        console.log(url);
        this.hvideo = false;
        this.stanhvideo = false;
        // this.audio = url;
        (document.getElementById('audioPlayback') as HTMLAudioElement).src = url;
        (document.getElementById('audioPlayback') as HTMLAudioElement).play();
        console.log('playing: ' + url);
        this.hablaVid = true;

        var msgDiv = document.getElementById("message_list");
        var vid = (document.getElementById('audioPlayback') as HTMLAudioElement);

        vid.onloadedmetadata = () => {
          console.log('durooooo', vid.duration);

          if (vid.duration === Infinity) {
            this.tiempo = 80;
          } else {
            this.tiempo = vid.duration;


          }
          setTimeout(() => {
            this.video = 'Cuadrado_Idle_3s.gif';

          }, this.tiempo * 1000);

        }
        console.log('---duration', vid);

        msgDiv.scrollTop = msgDiv.scrollHeight;
      }           // successful response
    });
  }

  audioSourceMeta($, audio) {
    console.log('Audioooooo', $, audio);

  }

  enviarAct(action, id, pd) {
    console.log(id);
    $('#' + id + 'bot' + pd).parent().addClass('mataclicks');

    const messagejso = {
      'type': 'message',
      'user': this.userData.userapi.user_id,
      'chanel': 'socket',
      'text': action.payload
    };



    this.mensajeChat.push({
      tipo: 'usuario',
      mensaje: action.payload,
      videoID: 'no',
      type: '',
      quick_replies: [],
      file: null
    });

    const myJSON = JSON.stringify(messagejso);
    this.ws.send(myJSON);
    // console.log('Teheran' + action, this.userData.userapi.user_id);
    // this.ws.onopen = (e) => { console.log('Adios - status ' + JSON.stringify(e)); };
    this.speechData = '';
    action = [];
    this.sp.DestroySpeechObject();


  }
  mude() {
    this.imgaudio = (this.imgaudio === 'audio') ? 'audiomute' : 'audio';

    if (this.imgaudio === 'audiomute') {
      (document.getElementById('audioPlayback') as HTMLAudioElement).pause();
      $('audio').stop();
      this.video = 'Cuadrado_Idle_3s.gif';
    } /*else {
      (document.getElementById('audioPlayback') as HTMLAudioElement).play();
      this.video = 'Cuadrado_Hablando_3s.gif';
    }*/

  }

  sendMsg(sms: any) {
    console.log(sms, sms.length);
    if (sms !== undefined && sms !== '') {
      if (this.speechData !== '' || this.speechData !== undefined) {
        const messagejso = {
          'type': 'message',
          'user': this.userData.userapi.user_id,
          'chanel': 'socket',
          'text': sms
        };


        this.mensajeChat.push({
          tipo: 'usuario',
          mensaje: this.speechData,
          videoID: 'no',
          type: '',
          quick_replies: null,
          file: null
        });

        const myJSON = JSON.stringify(messagejso);
        this.ws.send(myJSON);
        // console.log('Teheran', this.userData.userapi.user_id);
        // this.ws.onopen = (e) => { console.log('Adios - status ' + JSON.stringify(e)); };
        this.speechData = '';

      }

      var msgDiv = document.getElementById("message_list");
      msgDiv.scrollTop = msgDiv.scrollHeight;

      this.habilitepho();
    } else {
      this.audioVoice('Escribe algo para responderte');
      this.video = 'Cuadrado_Hablando_3s.gif';

    }


  }

  botid() {


  }
  onResizeh8(e) {
    console.log('8', e)
  }
  onResizeh4(e) {
    console.log('4', e)
  }
  onMetadataEntry(e, video) {
    this.loaded++;
    // console.log('metadata: ', e);
    // console.log('duration: ', video.duration);
    // console.log('duration----: ', this.loaded);

    // (document.getElementById('vid_entry') as HTMLAudioElement).play();
    // $('#vid_entry').get(0).play();
  }

  OnendedEntry(e, video) {
    this.hvideo = false;
    this.hablaVid = false;
    this.stanhvideo = true;
    setTimeout(() => {

      (document.getElementById('vid_entry1') as HTMLAudioElement).play();
    }, 300);

    // console.log('metadata: ', e);
  }

  onMetadataEntryS(e, video) {
    this.hvideo = false;
    this.hablaVid = false;
    this.stanhvideo = true;

    // console.log('metadata: ', e);
  }

  OnendedEntryS(e, video) {
    this.hvideo = false;
    this.hablaVid = false;
    this.stanhvideo = true;
    setTimeout(() => {

      (document.getElementById('vid_entry1') as HTMLAudioElement).play();
    }, 300);
    // console.log('metadata: ', e);
  }
  onMetadataEntryH(e, video) {
    // console.log('metadata: ', e);
  }
  OnendedEntryH() {
    this.hvideo = false;
    this.hablaVid = false;
    this.stanhvideo = true;
    setTimeout(() => {

      (document.getElementById('vid_entry1') as HTMLAudioElement).play();
    }, 300);
  }
  chekload() {
    if (this.loaded === 2) {
      $('#dimScreen').hide();
      $('#vid_entry').get(0).play();
      // document.getElementById("vid_entry").play()
    }
  }
  activateSpeechSearchMovie(): void {
    this.showSearchButton = false;
    this.sp.record()
      .subscribe(
        // listener
        (value) => {
          this.speechData = value;

          //
          // console.log(value);
        },
        // errror
        (err) => {
          // console.log(err);
          if (err.error === 'no-speech') {
            //  console.log('--restatring service--');
            this.activateSpeechSearchMovie();
            this.sp.DestroySpeechObject();
          }
        },
        // completion
        () => {
          this.showSearchButton = true;
          console.log('--complete--');
          this.sendMsg(this.speechData);
          // this.activateSpeechSearchMovie();
        });
  }
  habilitepho() {
    this.showSearchButton = true;
  }

  getMensajes() {
    this.servicio.sendgetAuth('https://kai.kioapps.com/api/pendingmessages?user=' + this.userData.userapi.user_id, this.userData.userapi.authorization).subscribe(
      (res: any) => {
        console.log('Mensajes', res);
      },
      error => {
        console.log('di error', error);
      });
  }

  Adios() {
    this.sp.DestroySpeechObject();
    this.servicio.borroTodo();
    this.ws.onclose = (e) => { console.log('Adios - status ' + JSON.stringify(e)); };
    this.mensajeChat = [];
  }
  ngOnDestroy() {
    this.sp.DestroySpeechObject();
    this.servicio.borroTodo();
    this.ws.onclose = (e) => { console.log('Adios - status ' + JSON.stringify(e)); };
    this.mensajeChat = [];
  }






}