import { BotComponent } from './bot.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from './../shared/shared.module';
import { BotRoutingModule } from './bot-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [BotComponent],
  imports: [
    CommonModule,
    SharedModule,
    BotRoutingModule,
    FormsModule, ReactiveFormsModule
  ]
})
export class BotModule { }
