import { SlidesComponent } from './slides.component';
import { SharedModule } from './../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



import { SlidesRoutingModule } from './slides-routing.module';

@NgModule({
  declarations: [SlidesComponent],
  imports: [
    CommonModule,
    SlidesRoutingModule,
    SharedModule
  ]
})
export class SlidesModule { }
