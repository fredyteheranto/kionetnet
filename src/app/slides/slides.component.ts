import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-slides',
  templateUrl: './slides.component.html',
  styleUrls: ['./slides.component.scss']
})
export class SlidesComponent implements OnInit {
  public rol = false;
  public slides = [
    {
      foto: 'assets/images/slide/slide0.png',
      texto: 'Sube el volúmen',
      text: 'Para una experiencia total',
      izq: false
    },
    {
      foto: 'assets/images/slide/slide1.png',
      texto: 'KAI Accedera a tu microfono, podrás interactuar mediante comandoz de voz',
      text: '',
      izq: true
    },
    {
      foto: 'assets/images/slide/slide2.png',
      texto: 'Gracias a mi tecnología NLU (Procesamiento de Lenguaje Natural) puedes interactuar conmigo vía chat o comandos de voz.',
      text: '',
      izq: false
    }
  ];
  constructor(public router: Router) { }

  ngOnInit() {

  }
  volver() {
    this.router.navigate(['/']);
  }

}
