import { AuthServic } from './../../auth/auth.service';

import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.reducer';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import { Injectable } from '@angular/core';


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styles: []
})
@Injectable({
  providedIn: 'root'
})
export class NavbarComponent implements OnInit, OnDestroy {
  @Input() public rol: boolean;
  nombre: string;
  subscription: Subscription = new Subscription();
  public navigation = [

    {
      ruta: null,
      name: 'Home',
      estado: '',
      parametro: '',
      class: ''
    },
    {
      ruta: null,
      name: 'F.A.Q',
      estado: '',
      parametro: 'faqs',
      class: ''
    },
    {
      ruta: null,
      name: 'Mensajes',
      estado: '',
      parametro: 'mensajes',
      class: ''
    },

    {
      ruta: '/',
      name: 'Logout',
      estado: '',
      parametro: 'salir',
      class: ''
    }
  ];


  constructor(private store: Store<AppState>, public serv: AuthServic) { }

  ngOnInit() {
    this.subscription = this.store.select('auth')
      .pipe(
        filter(auth => auth.user != null)
      )
      .subscribe(auth => this.nombre = auth.user.nombre);


    // if (!this.serv.estado) {
    //   this.serv.logout();
    // }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  vef() {
    console.log('ok', this.serv.estado);
    if (!this.serv.estado) {
      this.serv.logout();
    }
  }
}

