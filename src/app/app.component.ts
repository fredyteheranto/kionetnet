import { Component, OnInit } from '@angular/core';
import { AuthServic } from './auth/auth.service';
import { MessagingService } from './shared/messaging.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  message;
  constructor(public authService: AuthServic, private messagingService: MessagingService) { }


  ngOnInit() {
   // this.authService.initAuthListener();
    const userId = 'user001';
    this.messagingService.requestPermission(userId);
    this.messagingService.receiveMessage();
    this.message = this.messagingService.currentMessage;
  }

}
