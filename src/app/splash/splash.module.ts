import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SplashComponent } from './splash.component';
import { SocialLoginModule, AuthServiceConfig } from 'angularx-social-login';
import { GoogleLoginProvider } from 'angularx-social-login';

import { SplashRoutingModule } from './splash-routing.module';

const config = new AuthServiceConfig([
  {
    id: GoogleLoginProvider.PROVIDER_ID,
    provider: new GoogleLoginProvider('483840403014-8odlh56teu58j2ecrarv8li95qlksi0i.apps.googleusercontent.com')

  }
]);
export function provideConfig() {
  return config;
}

@NgModule({
  declarations: [SplashComponent],
  imports: [
    CommonModule,
    SocialLoginModule,
    FormsModule, ReactiveFormsModule,
    SplashRoutingModule
  ], providers: [
    {
      provide: AuthServiceConfig,
      useFactory: provideConfig
    }
  ]
})
export class SplashModule { }
