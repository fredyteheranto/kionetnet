import { AppState } from './../app.reducer';
import { AuthServic } from './../auth/auth.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { AuthService, SocialUser } from 'angularx-social-login';
import { GoogleLoginProvider } from 'angularx-social-login';




declare var $: any;

@Component({
  selector: 'app-splash',
  templateUrl: './splash.component.html',
  styleUrls: ['./splash.component.scss']
})
export class SplashComponent implements OnInit, OnDestroy {
  public rutaGet;
  public accion;
  public form = {
    login: false,
    registro: false,
    olvideclave: false
  };
  public registro = {
    nombre: '',
    empresa: '',
    telefono: '',
    correo_electronico: '',
    contrasena: '',
    Confirmar_contrasena: ''
  };
  public ingreso = {
    email: '',
    password: ''
  };
  public lostaccount = {
    emaillost: ''
  };
  private user: SocialUser;
  private loggedIn: boolean;

  cargando: boolean;
  subscription: Subscription;
  public titulotermino = 'AVISO DE PRIVACIDAD';
  public contete = false;
  constructor(private route: ActivatedRoute, public authService: AuthServic, private router: Router,
    private store: Store<AppState>, private authServices: AuthService) {

    this.route.queryParams.subscribe(params => {
      const t = this;
      $(document).ready(() => {
        if (params.login === 'on') {
          const link = document.getElementById('login');
          link.click();
        } else { }
        if (params.registro === 'on') {
          const link = document.getElementById('registro');
          link.click();
        } else { }
        if (params.link === 'salir') {
          this.loggedIn = false;
          this.authServices.signOut();
          this.subscription.unsubscribe();

          this.user = null;
          this.signOut();

        } else {
          this.loggedIn = false;
          this.authServices.signOut();
          this.subscription.unsubscribe();

          this.user = null;
          this.signOut();
        }

      });

    });
    const cleanUri = location.protocol + '//' + location.host + location.pathname;
    window.history.replaceState({}, document.title, cleanUri);
  }

  ngOnInit() {
    this.subscription = this.store.select('ui').subscribe(ui => this.cargando = ui.isLoading);




    this.authServices.authState.subscribe((user) => {
      this.user = user;
      this.loggedIn = (user != null);
     // console.log(this.loggedIn, 'Google', user);


      // setTimeout(() => {
      //   if (this.loggedIn) {
      //     console.log('oooooooo');

      //     document.location.href = window.location.href + '/#/bot';
      //   }
      // }, 800);

    });



    //  });
  }
  checkaviso(e) {
    this.titulotermino = (e === 'termino') ? 'KIO NETWORKS TERMINOS Y CONDICIONES KAI' : 'AVISO DE PRIVACIDAD';
    this.contete = (e === 'termino') ? true : false;

  }
  onlogin() {
    this.form.login = true;
    this.form.registro = false;
    this.form.olvideclave = false;
    this.accion = 'Log In';

  }
  onregister() {
    this.form.login = false;
    this.form.registro = true;
    this.form.olvideclave = false;
    this.accion = 'Registro';
  }
  onlostPAss() {
    this.form.login = false;
    this.form.registro = false;
    this.form.olvideclave = true;
    this.accion = 'Recuperar Contraseña';
  }
  onSubmit(accion) {
    console.log(accion);
    this.cargando = true;
    switch (accion) {
      case 'Recuperar Contraseña':
        const emaillow = this.lostaccount.emaillost.split('@');
        const domlow = (emaillow[1] === undefined) ? undefined : emaillow[1].split('.');
        if (this.lostaccount.emaillost && domlow !== undefined) {
          const lostsend = {
            email: this.lostaccount.emaillost
          };
          this.authService.senPost('forgot', lostsend).subscribe(
            (res: any) => {
              console.log('dara', res);
              if (res.success) {
                Swal('Recuperar Contraseña', res.msg, 'info');

              } else {
                Swal('Recuperar Contraseña', res.msg, 'error');

              }

            },
            error => {
              Swal('Error', error.error.msg, 'error');
              console.log('dara', error);
            });

        } else {
          Swal('Error', 'Digite un correo valido', 'error');

        }
        this.lostaccount = {
          emaillost: ''
        };

        break;
      case 'Registro':
        const email = this.registro.correo_electronico.split('@');
        const dom = (email[1] === undefined) ? undefined : email[1].split('.');
        if (dom === undefined) {
          Swal('Error en el Registro', 'Digite un email valido', 'error');
          this.cargando = false;
        } else {
          if (this.registro.contrasena === this.registro.Confirmar_contrasena) {
            const regis = {
              name: this.registro.nombre,
              company: this.registro.empresa,
              phone: this.registro.telefono,
              email: this.registro.correo_electronico,
              pass: this.registro.contrasena,
            };

            this.authService.senPost('singin', regis).subscribe((res: any) => {
              console.log('dara', res);

              if (res.success) {
                Swal('Registro', res.msj, 'info');
                // this.authService.crearUsuario(this.registro);

              } else {
                Swal('Registro', res.msj, 'error');

              }
              $('#ModalReg').modal('hide');
            },
              error => {
                Swal('Error', error.error.msg, 'error');
                console.log('dara', error);
              });

            $('#ModalReg').modal('hide');
          } else {
            Swal('Error en el Registro', 'Las contraseñas no coinciden', 'error');
            this.cargando = false;
          }
        }
        this.registro = {
          nombre: '',
          empresa: '',
          telefono: '',
          correo_electronico: '',
          contrasena: '',
          Confirmar_contrasena: ''
        };
        break;
      case 'Log In':
        const emaillo = this.ingreso.email.split('@');
        const domlo = (emaillo[1] === undefined) ? undefined : emaillo[1].split('.');
        if (this.ingreso.email && this.ingreso.password && domlo !== undefined) {

          const send = {
            autentication: 'UserPass',
            user_id: this.ingreso.email,
            user_pass: this.ingreso.password,
            idtoken: ''
          };
          const dev = {
            email: this.ingreso.email,
            password: this.ingreso.password
          };



          this.authService.senPost('login', send).subscribe((res: any) => {
            console.log(res);
            this.authService.estado = true;
            //  this.authService.login(this.ingreso.email.toString().trim(), this.ingreso.password);
            this.authService.SaveStorage('LoginAPi', res);
            console.log('dara', res, dev);

            $('#ModalReg').modal('hide');
            this.router.navigate(['/bot']);
          },
            error => {
              Swal('Error', error.error.msg, 'error');
              this.authService.estado = false;
              console.log('dara', error);
            });
        } else {
          Swal('Error en el login', 'Usuario o contraseña erroneos', 'error');
          this.authService.estado = false;
        }
        this.ingreso = {
          email: '',
          password: ''
        };


        /*
        para la duda que me comentabas, al momento de realizar login con estos accesos (user: kiotron_test2@kionetworks.com
        password: ZaxscD), el API regresa un json con esta info

        */


        // this.authService.login(this.ingreso.email, this.ingreso.password);

        //  this.ingreso;
        break;
    }
    this.cargando = false;
    $('#ModalReg').modal('hide');

  }
  loginAsgoogle() {
    //  this.authService.signInWithGoogle();
    $('#ModalReg').modal('hide');
    this.authService.estado = true;
  }
  signInWithGoogle(): void {
    console.log('iiii');
    this.authServices.signIn(GoogleLoginProvider.PROVIDER_ID);
    this.authServices.authState.subscribe((user) => {
      this.user = user;
      this.loggedIn = (user != null);
      console.log(user, 'Google');
      this.authService.signInWithGoogle(user);
      $('#ModalReg').modal('hide');

    });
  }
  signOut() {
    this.user = null;
    this.loggedIn = null;

    this.authService.estado = false;

    this.authService.logout();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
