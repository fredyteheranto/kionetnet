
import { Injectable, NgZone } from '@angular/core';
import { Observable } from 'rxjs';


import * as _ from 'lodash';

interface IWindow extends Window {
  webkitSpeechRecognition: any;
  SpeechRecognition: any;
}

@Injectable()
export class KioService {
  speechRecognition: any;

  constructor(private zone: NgZone) {
  }

  record(): Observable<string> {
    if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
      console.log('getUserMedia supported.');
      navigator.mediaDevices.getUserMedia(
        // constraints - only audio needed for this app
        {
          audio: true
        })

        // Success callback
        .then(function (stream) {


        })

        // Error callback
        .catch(function (err) {
          console.log('The following getUserMedia error occured: ' + err);
        }
        );
    } else {
      console.log('getUserMedia not supported on your browser!');
    }

    return Observable.create(observer => {
      const { webkitSpeechRecognition }: IWindow = window as IWindow;
      this.speechRecognition = new webkitSpeechRecognition();
      // this.speechRecognition = SpeechRecognition;
      this.speechRecognition.continuous = true;
      // this.speechRecognition.interimResults = true;
      this.speechRecognition.lang = 'es-MX';
      this.speechRecognition.maxAlternatives = 1;

      this.speechRecognition.onresult = speech => {
        let term = '';
        if (speech.results) {
          const result = speech.results[speech.resultIndex];
          const transcript = result[0].transcript;
          if (result.isFinal) {
            if (result[0].confidence < 0.3) {
              console.log('Unrecognized result - Please try again');
              this.speechRecognition.stop();
            } else {
              term = _.trim(transcript);
              console.log('Did you said? -> ' + term + ' , If not then say something else...');
              this.speechRecognition.stop();
            }
          }
        }
        this.zone.run(() => {
          observer.next(term);
        });
      };

      this.speechRecognition.onerror = error => {
        observer.error(error);
        this.speechRecognition.stop();
      };

      this.speechRecognition.onend = () => {
        observer.complete();

      };

      this.speechRecognition.start();
      console.log('Say something - We are listening !!!');
    });
  }

  DestroySpeechObject() {
    if (this.speechRecognition) {
      this.speechRecognition.stop();
    }
  }

}
